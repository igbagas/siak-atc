@extends('layout.main')

@section('title', 'SIAK | MATA PELAJARAN')

@section('content')

<div class="col-lg-12 mb-4">
    <!-- Simple Tables -->
    <div class="card">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
        <h6 class="m-0 font-weight-bold text-primary">DATA MATA PELAJARAN</h6>
        <a class="m-0 float-right btn btn-success btn-sm" href="{{ route('subject.create') }}">
            <i class="fas fa-plus"></i> Tambah Mata Pelajaran
        </a>
    </div>
    <hr class="sidebar-divider my-0">
    <div class="table-responsive">
        @if(session()->has('success'))
            <div class="alert alert-success" role="alert">
                {{session()->get('success')}}
            </div>
        @endif
        <table class="table align-items-center table-flush">
        <thead class="thead-light">
            <tr>
            <th >No</th>
            <th >Nama</th>
            <th >Pilihan</th>
            </tr>
        </thead>
        <tbody>
            @if(count($subjects) != 0)
            @foreach($subjects as $index => $subject)
            <tr>
                <td>{{$index + $subjects->firstItem()}}</td>
                <td>{{$subject->name}}</td>
                <td>
                    <div class="btn-group btn-group-sm" role="group" aria-label="...">
                        <a href="{{route('subject.edit', $subject->id)}}" class="btn btn-success btn-sm">
                            <i class="fas fa-edit"></i>
                        </a>
                        <a href="{{route('subject.destroy', $subject->id)}}" class="btn btn-danger btn-sm btn-delete">
                            <i class="fas fa-trash"></i>
                        </a>
                    </div>
                </td>
            </tr>
            @endforeach
            <form id="form-delete" method="POST" style="display: none">
                @csrf
                @method('DELETE')
            </form>
            @else
                <tr>
                    <td></td>
                    <td align="center">Tidak Ada Data</td>
                    <td></td>
                </tr>
            @endif
        </tbody>
        </table>
        <hr class="sidebar-divider my-0">
        <nav class="mt-4 d-flex justify-content-center">
            {{ $subjects->links() }}
        </nav>

    </div>
</div>
@endsection

@section('scripts')
<script>
    document.querySelectorAll('.btn-delete').forEach((button) => {
        button.addEventListener('click', function(event) {
            event.preventDefault()
            if (confirm("Are You Sure ?")) {
                let action = this.getAttribute('href')
                let form = document.getElementById('form-delete')
                form.setAttribute('action', action)
                form.submit()
            }
        })
    })
</script>
@endsection
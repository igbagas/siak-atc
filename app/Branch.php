<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Teacher;

class Branch extends Model
{
   //allow mass assignment
   protected $fillable = ['name'];

    //relationship method
    public function teachers()
    {
        return $this->hasMany(Teacher::class);
    }
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;
use App\Branch;
use App\Subject;

class TeacherController extends Controller
{
    public function index()
    {
        $branches = Branch::orderBy('name')->pluck('name', 'id')->prepend('SEMUA CABANG','');
        $subjects = Subject::orderBy('name')->pluck('name', 'id');
        $teachers = Teacher::orderBy('teacher_id', 'asc')->where(function($query) {
            if($branchId = request('branch_id')){
                $query->where('branch_id', $branchId);
            }
        })->paginate(5);
        return view('teacher.index', compact('teachers', 'branches', 'subjects'));
    }

    public function create()
    {
        $branches = Branch::orderBy('name')->pluck('name', 'id')->prepend('PILIH CABANG', '');
        return view('teacher.create', compact('branches'));
    }


    public function store(Request $request) {

        $this->validate($request, [
            'teacher_id' => 'required|unique:teachers',
            'fullname' => 'required|alpha',
            'phone' => 'required',
            'address' => 'required'



        ]);
    }

    public function detail($id)
    {
        $teacher = Teacher::findOrFail($id);
        return view('teacher.show', compact('teacher'));
    }

    public function destroy($id)
    {
        $teacher = Teacher::findOrFail($id);
        $teacher->delete();

        return back()->with('message', 'Data Pengajar Berhasil Dihapus');
    }
}

<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class BranchesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $branches = [];

        $faker = Faker::create();

        for($i = 0; $i < 10; $i++)
        {
            $branches[] = [
                'brn_name' => $faker->name,
                'created_at' => now(),
                'updated_at' => now()
            ];
        }

        DB::table('branches')->insert($branches);
    }
}

<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('teacher', 'TeacherController@index')->name('teacher');

Route::get('teacher/create', 'TeacherController@create')->name('create.teacher');

Route::post('teacher', 'TeacherController@store')->name('store.teacher');

Route::get('teacher/{id}', 'TeacherController@detail')->name('show.teacher');

Route::delete('teacher/{id}','TeacherController@destroy')->name('delete.teacher');

Route::resource('branch', 'BranchController');

Route::resource('subject', 'SubjectController');

